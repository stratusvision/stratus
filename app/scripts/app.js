var Stratus = window.Stratus = Ember.Application.create();

Ember.set('Stratus.LocalUser', JSON.parse(localStorage.getItem("ember_parse_user")));

Stratus.ApplicationAdapter = DS.ParseAdapter.extend({
    applicationId: 'EB138VdZJ308yRWtiZHCv6MT9AqoxFBvKDQwPY3V',
    restApiId: 'QYZ3huVqlHoXnQjDvXv22ZkCoLcvdkyHb7jsjMgA',
    javascriptId: 'tjEek0jYhkH1TTMgj48H6Al8qAmxfaOaBdA0lIvb'
});

Stratus.CurrentUserHelper = {
    beforeModel: function() {        
        if(!localStorage.getItem("ember_parse_user")) {               
            this.controllerFor('application').transitionToRoute('application');
        }
        //when you click a nav link from a mobile view, the menu doesn't collapse by default
        if($('button.navbar-toggle').is(':visible')) {
            console.log('loading mobile route');
            $('.navbar-collapse').collapse('hide');
        } 
    } 
};   

Parse.initialize("EB138VdZJ308yRWtiZHCv6MT9AqoxFBvKDQwPY3V", "tjEek0jYhkH1TTMgj48H6Al8qAmxfaOaBdA0lIvb");

/* Order and include as you please. */
require('scripts/views/*');
require('scripts/router');
require('scripts/routes/*');
require('scripts/models/*');
require('scripts/store');
require('scripts/controllers/*');