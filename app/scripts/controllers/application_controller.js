Stratus.ApplicationController = Ember.Controller.extend({
    currentUser: function() {       
        var localUser = Ember.get('Stratus.LocalUser');
        if(localUser) { 
            return this.get('store').find('user', localUser.userId);
        } else return false;
    }.property('Stratus.LocalUser'),
    actions: {
        logout: function() {
            var controller = this;
            controller.get('currentUser')
                .then(function(user){
                    user.logout();
                    Ember.set('Stratus.LocalUser', false);
                    controller.transitionToRoute('application');
                });
        },
        scroll: function(id) {
            if(this.currentRouteName === 'index') {
                $("html, body").animate({ scrollTop: $("#"+id).offset().top }, 1000);
            } else {
                this.transitionToRoute('index');
            }
        }
    } 
});

   