Stratus.CameraEditController = Ember.ObjectController.extend({
    is_new: true,
    currentUser: function() {       
        var localUser = Ember.get('Stratus.LocalUser');
        if(localUser) { 
            return this.get('store').find('user', localUser.userId);
        } else return false;
    }.property('Stratus.LocalUser'),
    title: function() {
        return (this.get('is_new')) ? 'Add a camera' : 'Edit Camera';
    }.property('is_new'),
    actionTitle: function() {
        return (this.get('is_new')) ? 'Submit' : 'Save';
    }.property('is_new'),
    actions: { 
        save: function(){
            var controller = this;
            var camera;
            var user = controller.get('currentUser');
            if(user && controller.get('is_new')) {
                console.log(user.get('id'));
                camera = user.get('cameras').createRecord();
                camera.set('createdBy', user.get('id')); 
            } else {
                camera = controller.get('model');
            }
            camera.set('name', controller.get('name')); 
            camera.set('streamkey', camera.get('createdBy') + '_' + controller.get('streamkey')); 
            camera.save().then(function(camera) {
                controller.transitionToRoute('cameras'); 
            });   
        }
    }
}); 

   