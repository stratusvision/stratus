Stratus.CamerasEditController = Ember.ObjectController.extend({
    actions: { 
        delete: function(camera){
            var controller = this;
			var confirmed = window.confirm('Are you sure you want to delete this camera?');
			if(confirmed === true) {
				camera.deleteRecord();
				camera.save();
				console.log(camera);
			}			
        }
    }
}); 

  