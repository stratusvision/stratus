Stratus.IndexController = Ember.Controller.extend({
    queryParams: 'scroll',
    mailSuccess: false,
    mailError: false,
    actions: { 
        sendMail: function(){
            var name = $('input#name').val();
            var email = $('input#email').val();
            var message = $('textarea#message').val();
            var website = $('input#website').val();
            var $button = $('button#sendMail');
            var controller = this;
            
            $button.attr('disabled', 'disabled');
            
            if(!website) {
                Parse.Cloud.run('mail', {name:name, email:email, message:message}, {
                  success: function(result) {
                      console.log(result);
                      controller.set('mailSuccess', true);
                  },
                  error: function(error) {
                      console.log(error);
                      controller.set('mailError', true);
                  }
                });
            } else {
                console.log('get outta here, spammo');   
            }
        }
    }
});

     