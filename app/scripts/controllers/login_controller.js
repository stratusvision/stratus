Stratus.LoginController = Ember.Controller.extend({
    username: null,
    password: null,
    loggedIn: false,
    loginMessage: null,
    actions: {
      login: function(){
        var controller = this;
        DS.ParseUserModel.login(this.get('email'), this.get('password')).then(
          function(user){
            controller.set('loggedIn', true);
            controller.set('loginMessage', "Welcome!");  
            Ember.set('Stratus.LocalUser', JSON.parse(localStorage.getItem("ember_parse_user")));
            controller.transitionToRoute('cameras');
          },
          function(error){
            controller.set('loggedIn', false);
            controller.set('loginMessage', 'Error logging in');
          }
        );
      }
    }
  }); 