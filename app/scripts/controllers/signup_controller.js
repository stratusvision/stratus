Stratus.SignupController = Ember.Controller.extend({
    username: null,
    password: null,
    email: null,
    loggedIn: false,
    loginMessage: null,
    actions: {
      signup: function(){
        var controller = this;
        Stratus.User.signup(this.get('email'), this.get('password'), this.get('email')).then(
          function(user){
            controller.set('loggedIn', true);
            controller.set('loginMessage', "Welcome!");
          },
          function(error){
            controller.set('loggedIn', false); 
            controller.set('loginMessage', 'Error signing up');
          }
        );
      }
    }
});

