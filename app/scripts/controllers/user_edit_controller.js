Stratus.UserEditController = Ember.ObjectController.extend({
    //needs: 'user',
    saved: null,
    actions: {
        save: function(){
            var controller = this;
            var user = controller.get('model');
            user.set('email',       controller.get('email')); 
            user.set('username',    controller.get('email')); 
            user.set('company',     controller.get('company')); 
            user.set('firstName',   controller.get('firstName')); 
            user.set('lastName',    controller.get('lastName')); 
            user.save().then(function(user) {
                controller.set('saved', true); 
            }); 
        }
    }
});

