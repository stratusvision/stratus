/*global Ember*/
Stratus.Camera = DS.ParseModel.extend({
    user: DS.belongsTo('user'),
    createdBy: DS.attr('string'),
    name: DS.attr('string'),
    streamkey: DS.attr('string')
});
 