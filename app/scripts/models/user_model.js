Stratus.User = DS.ParseUserModel.extend({
    cameras: DS.hasMany('camera'),
    username: DS.attr('string'),
    email: DS.attr('string'),
    firstName: DS.attr('string'),
    lastName: DS.attr('string'),
    company: DS.attr('string')
});
