Stratus.Router.map(function () {
    
    this.resource('camera', { path: 'camera/:camera_id' }); 
    this.resource('camera.edit', { path: 'camera/:camera_id/edit' });
    this.resource('cameras.new', { path: 'cameras/new' });
    this.resource('cameras.edit', { path: 'cameras/edit' });
    this.resource('cameras');

    this.resource('user', { path: 'user/:user_id' }); 
    this.resource('user.edit', { path: 'user/:user_id/edit' });
    this.resource('users.create', { path: 'users/new' });
    
    this.route('archive', { path: 'archive/:archive_id' });
    this.route('archives', { path: 'archives/:streamkey' });
    this.route('signup');
    this.route('login');
  
});
      