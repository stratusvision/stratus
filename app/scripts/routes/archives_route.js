Stratus.ArchivesRoute = Ember.Route.extend(Stratus.CurrentUserHelper, {
    setupController: function(controller, model) {
        console.log(model);
        controller.set('streamkey', model);
        $.ajax('http://stratus-restify.elasticbeanstalk.com/archives/'+model, {
            crossDomain: true,
            dataType: 'json',
            error: function() {
                controller.set('archives', false);
            },
            success: function(data, status, jqxhr) {
                controller.set('archives', data.Contents);
                console.log(data);
            }
        });
        /*var archives = $.ajax('stratus-restify.elasticbeanstalk.com/archives', {
            data: {
                streamkey:   
            }
        });
        controller.set('archives', archives);*/
    },
    model: function(params) {
        return params.streamkey;
    }
});

