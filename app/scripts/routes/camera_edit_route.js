Stratus.CameraEditRoute = Ember.Route.extend(Stratus.CurrentUserHelper, {
    setupController: function(controller, model) {
        if(model.id) {
            controller.set('is_new', false);
        }
        controller.set('model', model);
    }
});

