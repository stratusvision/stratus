Stratus.CameraRoute = Ember.Route.extend(Stratus.CurrentUserHelper, {
  model: function(params) {
    return this.get('store').find('camera', params.camera_id);
  }
});

