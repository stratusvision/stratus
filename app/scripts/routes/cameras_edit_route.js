Stratus.CamerasEditRoute = Ember.Route.extend(Stratus.CurrentUserHelper, {
    model: function() {
        var local_user = Ember.get('Stratus.LocalUser');  
        return this.get('store').find('camera', {where: {createdBy: local_user.userId}});
    }
});

 