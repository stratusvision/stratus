Stratus.CamerasNewRoute = Ember.Route.extend(Stratus.CurrentUserHelper, {
    controllerName: 'camera.edit',
    model: function() {
        var local_user = Ember.get('Stratus.LocalUser');  
        return this.get('store').find('user', local_user.userId);
    },
    setupController: function(controller, model) {
        this.controllerFor('camera.edit').setProperties({ is_new: true, model: model.get('cameras').createRecord(), currentUser: model });
    },
    renderTemplate: function() {
        this.render('camera/edit');         
    }
});   