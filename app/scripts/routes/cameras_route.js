Stratus.CamerasRoute = Ember.Route.extend(Stratus.CurrentUserHelper, {
    model: function() {
        var local_user = Ember.get('Stratus.LocalUser');  
        //console.log(this.get('store').findHasMany('camera'));
        return this.get('store').find('camera', {where: {createdBy: local_user.userId}});
    }
});

 