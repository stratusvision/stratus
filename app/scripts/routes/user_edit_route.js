Stratus.UserEditRoute = Ember.Route.extend(Stratus.CurrentUserHelper, {
  model: function(params) {
    return this.get('store').find('user', params.user_id);
  }
});

