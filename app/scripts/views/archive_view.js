Stratus.ArchiveView = Ember.View.extend({
    didInsertElement: function() { 
        $('.video').each(function() {
            var id = $(this).attr('id');
            var stream = $(this).data('stream-key');            
            jwplayer(id).setup({
                primary: 'flash',
                //fallback: false,
                aspectratio: "16:9",
                width: "100%",
                skin: "bekle",
                autostart: true,
                sources: [/*{
                    file: "http://stream.stratusvisioncctv.com/live/"+stream+"/manifest.f4m",
                },*/ {
                    file: "http://stream.stratusvisioncctv.com/vod/mp4:"+stream+"/playlist.m3u8",
                }, {
                    file: "http://stream.stratusvisioncctv.com/vod/mp4:"+stream+"/manifest.f4m"
                }]
            });
        });
    }
});
